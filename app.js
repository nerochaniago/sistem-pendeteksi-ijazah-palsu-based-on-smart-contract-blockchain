const express = require('express')
const fileUpload = require('express-fileupload')
const path = require('path')
const bodyParser = require('body-parser')
const Joi = require('joi')
const fs = require('fs')
const hash = require('sha256')
const nodemailer = require('nodemailer');
const wkhtmltopdf = require('wkhtmltopdf');

let port = 8000

const app = express()


app.use('/public', express.static(path.join(__dirname, 'static')))

app.use(fileUpload())

app.use(bodyParser.urlencoded({
    extended: false
}))

app.set('view engine', 'ejs')

app.get('/', (req, res) => {
    res.render('index')
})

app.get('/admin', (req, res) => {
    res.render('admin')
})


app.get('/transkrip', (req, res) => {
    res.render('transkrip')
})


app.get('/approval', (req, res) => {
    res.render('approval')
})


app.post('/transkrip', (req, res) => {
    const schema = Joi.object().keys({
        ipk: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        lembaga: Joi.string().trim().required().error(() => {
            return {
                message: `Nama Lembaga harus diisi`
            }
        }),
        nomor: Joi.string().trim().required().error(() => {
            return {
                message: `Nama Lembaga harus diisi`
            }
        }),
        nomorRektor: Joi.string().trim().required().error(() => {
            return {
                message: `Nomor Rektor harus diisi`
            }
        }),
        nama: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        ttl: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        nim: Joi.number().integer().max(9999999999999).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "number.max":
                        err.message = `${err.context.key} maksimal 12 karakter`
                        break
                    case "number.base":
                        err.message = `${err.context.key} harus angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        // gelar: Joi.string().regex(/^([a-zA-Z])\.([a-zA-Z]+$)/).required(),
        prodi: Joi.string().alphanum().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.alphanum":
                        err.message = `${err.context.key} tidak boleh mengandung special character`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        rektor: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        nipRektor: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        dekan: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        nipDekan: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        jenjang: Joi.string().required().error(() => {
            return {
                message: `Jenjang harus diisi`
            }
        }),
        fakultas: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        tglLulus: Joi.date().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "date.base":
                        err.message = `${err.context.key} harus format tanggal yang sesuai`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        ta: Joi.string().required().error(() => {
            return {
                message: `Judul Tugas Akhir harus diisi`
            }
        }),
        kode_matkul: Joi.string(),
        matkul: Joi.string(),
        semester: Joi.string(),
        sks: Joi.number().integer(),
        nilai: Joi.string()
    })

    if (!req.files) {
        res.redirect('/transkrip')
    } else {
        const univ = req.files.logo
        const pas = req.files.pas

        const logoName = univ.mimetype == 'image/png' ? 'logoUniv.png' : null

        const pasName = pas.mimetype == 'image/jpg' ? 'pasFoto.jpg' : (pas.mimetype == 'image/jpeg' ? 'pasFoto.jpeg' : null)

        univ.mv('./static/img/' + logoName, (e) => {
            if (e) {
                res.send(e)
            }
        })

        pas.mv('./static/img/' + pasName, (e) => {
            if (e) {
                res.send(e)
            }
        })

        // console.log(req.body.kode_matkul)

        res.render('transkripNilai', {
            data: req.body
        })
    }
})

app.get('/blockchain', (req, res) => {
    res.render('blockchain')
})


app.get('/kirim_ijazah', (req, res) => {
    res.render('kirim_ijazah')
})

app.get('/kirim_transkrip', (req, res) => {
    res.render('kirim_transkrip')
})

app.post('/send_transkrip', (req, res) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'sivilchain@gmail.com',
            pass: '123Sivil' // naturally, replace both with your real credentials or an application-specific password
        }
    });
    const mailOptions = {
        from: 'sivilchain@gmail.com',
        to: req.body.email,
        subject: 'File Transkrip',
        text: req.body.message,
        attachments: [{
            filename: 'Transkrip.pdf',
            path: 'D:\\static\\document\\Transkrip.pdf',
            contentType: 'application/pdf'
        }]
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.render('transkrip')
        }
    });
})






app.post('/send_ijazah', (req, res) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'sivilchain@gmail.com',
            pass: '123Sivil' // naturally, replace both with your real credentials or an application-specific password
        }
    });
    const mailOptions = {
        from: 'sivilchain@gmail.com',
        to: req.body.email,
        subject: 'File Ijazah',
        text: req.body.message,
        attachments: [{
            filename: 'Ijazah.pdf',
            path: 'D:\\static\\document\\Document.pdf',
            contentType: 'application/pdf'
        }]
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.render('admin')
        }
    });
})

app.get('/cek', (req, res) => {
    // if (fs.existsSync('./static/document/Document.pdf')) {


    if (fs.existsSync('D:\\static\\document\\Document.pdf')) {
        // let dataBuffer = fs.readFileSync('./static/document/Document.pdf')
        let dataBuffer = fs.readFileSync('D:\\static\\document\\Document.pdf')
        res.render('cek', {
            hash: `0x${hash(dataBuffer)}`
        })
    } else {
        res.redirect('/admin')
    }

})




app.get('/cekTranskrip', (req, res) => {
    // if (fs.existsSync('./static/document/Transkrip.pdf')) {
    if (fs.existsSync('D:\\static\\document\\Transkrip.pdf')) {
        // let dataBuffer = fs.readFileSync('./static/document/Transkrip.pdf')
        let dataBuffer = fs.readFileSync('D:\\static\\document\\Transkrip.pdf')
        res.render('cek_transkrip', {
            hash: `0x${hash(dataBuffer)}`
        })
    } else {
        res.redirect('/admin')
    }
})

app.get('/stakeholder', (req, res) => {
    res.render('blockchainserver')
})








app.post('/sertifikat', (req, res) => {



    const schema = Joi.object().keys({
        jenjang: Joi.string().required().error(() => {
            return {
                message: `Jenjang harus diisi`
            }
        }),
        lembaga: Joi.string().trim().required().error(() => {
            return {
                message: `Nama Lembaga harus diisi`
            }
        }),
        nomor: Joi.string().trim().required().error(() => {
            return {
                message: `Nama Lembaga harus diisi`
            }
        }),
        nama: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        ttl: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.empty":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        nim: Joi.number().integer().max(9999999999999).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "number.max":
                        err.message = `${err.context.key} maksimal 12 karakter`
                        break
                    case "number.base":
                        err.message = `${err.context.key} harus angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        gelar: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        prodi: Joi.string().alphanum().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.alphanum":
                        err.message = `${err.context.key} tidak boleh mengandung special character`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        rektor: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        nipRektor: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        dekan: Joi.string().regex(/^([^0-9]+)$/).required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "string.regex.base":
                        err.message = `${err.context.key} tidak boleh mengandung angka`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        nipDekan: Joi.string().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
        tglLulus: Joi.date().required().error(errors => {
            errors.forEach(err => {
                switch (err.type) {
                    case "any.required":
                        err.message = `${err.context.key} harus diisi`
                        break
                    case "date.base":
                        err.message = `${err.context.key} harus format tanggal yang sesuai`
                        break
                    default:
                        break
                }
            })
            return errors
        }),
    })

    if (!req.files) {
        res.render('admin', {
            message: "Upload Foto"
        })
    } else {
        const univ = req.files.logo
        const pas = req.files.pas

        const logoName = univ.mimetype == 'image/png' ? 'logoUniv.png' : null

        const pasName = pas.mimetype == 'image/jpg' ? 'pasFoto.jpg' : (pas.mimetype == 'image/jpeg' ? 'pasFoto.jpeg' : null)

        univ.mv('./static/img/' + logoName, (e) => {
            if (e) {
                res.send(e)
            }
        })

        pas.mv('./static/img/' + pasName, (e) => {
            if (e) {
                res.send(e)
            }
        })

        Joi.validate(req.body, schema, (err, result) => {
            if (err) {
                const message = err.details[0].message
                res.render('admin', {
                    message: message
                })
            } else {
                res.render('sertifikat', {
                    data: req.body,
                    fs: fs
                })

            }


        })
    }



})





app.listen(port, '0.0.0.0', (e) => {
    if (!e) {
        console.log(`Ini Servernya Ya Pak http://localhost:${port}`)
    }
});